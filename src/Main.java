import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        List<String> stringList = new ArrayList<String>();
        stringList.add("One flew over the cuckoo's nest");
        stringList.add("Gone with the wind");
        stringList.add("The great expectations");
        stringList.add("To kill a muckingbird");
        stringList.add("Gone with the wind");

        Stream<String> stream;

        // Non-Terminal Operations

        System.out.println("----------- filter() : -----------");
        // filter()
        stringList
                .stream()
                .filter((value) -> {
                    return value.length() >= 20;
                })
                .forEach((value) -> System.out.println(value)); // One flew over the cuckoo's nest; The great expectations; To kill a muckingbird

        System.out.println("----------- map() : -----------");
        // map()
        stringList
                .stream()
                .map((value) -> {
                    return value.length();
                })
                .forEach((value) -> System.out.println(value)); // 31; 21; 18

        System.out.println("----------- flatMap() : -----------");
        // flatMap()
        stringList
                .stream()
                .flatMap((value) -> {
                    String[] split = value.split(" ");
                    return (Stream<String>) Arrays.asList(split).stream();
                })
                .forEach((value) -> { System.out.println(value); }); // One; flew; over; the; cuckoo's; nest


        System.out.println("----------- distinct() : -----------");
        // distinct()
        stringList
                .stream()
                .distinct()
                .forEach((value) -> { System.out.println(value); });

        System.out.println("----------- limit() : -----------");
        // limit()
        stringList
                .stream()
                .limit(2)
                .forEach((value) -> { System.out.println(value); });


        boolean result;

        // Terminal Operations

        System.out.println("----------- anyMatch() : -----------");
        // anyMatch()
        result = stringList
                .stream()
                .anyMatch((value) -> {
                    return value.length() > 20;
                });
        System.out.println("result - " + result);

        System.out.println("----------- allMatch() : -----------");
        // allMatch()
        result = stringList
                .stream()
                .allMatch((value) -> {
                    return value.length() > 10;
                });
        System.out.println("result - " + result);

        System.out.println("----------- noneMatch() : -----------");
        // noneMatch()
        result = stringList
                .stream()
                .noneMatch((value) -> {
                    return value.length() > 20;
                });
        System.out.println("result - " + result);

        System.out.println("----------- collect() : -----------");
        // collect()
        List<String> resCol = stringList
                .stream()
                .map(val -> {
                    return val.toUpperCase();
                })
                .collect(Collectors.toList());
        System.out.println("resCol - " + resCol);

        System.out.println("----------- count() : -----------");
        // count()
        long countRes = stringList
                .stream()
                .count();
        System.out.println("countRes - " + countRes);

        System.out.println("----------- reduce() : -----------");
        // reduce()
        String reduceRes = stringList
            .stream()
            .reduce((current, total) -> {
                return total +  "; " + current;
            })
            .get();
        System.out.println("reduceRes - " + reduceRes);

        System.out.println("----------- toArray() : -----------");
        // toArray()
        Object[] arrRes = stringList
                .stream()
                .map(val -> {
                    return val.length();
                })
                .toArray();
        System.out.println("arrRes - " + arrRes[1]);
    }
}
